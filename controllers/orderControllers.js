const User = require("../models/User");
const Product = require("../models/Product");
const Cart = require("../models/cart");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");





module.exports.getAllOrders= (request, response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	if(!userData.isAdmin){
			return response.json("Sorry! Only Admin is Authorize to access this page!")
		}else{
			return Order.find({}).then(result => response.json(result))
			.catch(err =>{
			response.json(err);
		})
	}
}

module.exports.getOrderCart= (request, response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	if(userData.isAdmin){
			return response.json("Sorry! Only regular user is Authorize to access this page!")
		}else{
			return Cart.find({userId:userData.id}).then(result => response.json(result))
			.catch(err =>{
			response.json(err);
		})
	}
}




module.exports.addToCart = async (request, response)=>{
	let token = request.headers.authorization;
	const userData = auth.decode(token);

	const quantity = request.body.quantity;
	
	// console.log(userData);
	if(!userData.isAdmin){
		const productDetails = await Product.findById(request.params.productId)
		console.log(productDetails);
		let totalAmount = quantity*productDetails.price;
		console.log(totalAmount)
		const cart = await Cart.create({
			userId:userData.id,
			productId:productDetails._id,
			productName : productDetails.productName,
			price : productDetails.price,
			quantity : quantity,
			totalAmount
		})
		return response.json("success");
		}else{
		response.json("Admin");
		}
	
}


module.exports.checkOut = async (request, response) =>{
	let token = request.headers.authorization;
	const userData = auth.decode(token);

	const {cartId} = request.body;
	console.log(cartId);
	if(!userData.isAdmin){
		const carts = await Cart.find({
			_id:cartId ,
		})
		
		console.log(carts)
		const products = [];
		   let totalAmount = 0;

		   
		   const productIds = [];
		  
		for (let i = 0; i < carts.length; i++) {
		     totalAmount += carts[i].totalAmount;
		     products.push({
		     	userId : carts[i].userId,
		       productId: carts[i].productId,
		       productName: carts[i].productName,
		       quantity: carts[i].quantity,
		       totalAmount 
		     });
		     productIds.push(carts[i].productId);
		}

		await Cart.deleteMany({ _id:cartId });

		const order = await Order.create(products);
		// console.log(order);
		return response.json(order);


	}else{
		response.json("Sorry! You are an admin.");
		}

}



	