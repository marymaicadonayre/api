const Product = require ("../models/Product");
const auth = require("../auth");


//Add/Create Product admin only
module.exports.addProduct = (request, response)=>{
	let token = request.headers.authorization;
	const userData = auth.decode(token);
	// console.log(userData);

	let newProduct = new Product ({
		productName:request.body.productName,
		description: request.body.description,
		price:request.body.price,
	})
	if(userData.isAdmin){
		newProduct.save().then(result => {
				console.log(result)
				response.send(true)
			}).catch(error => {
				console.log(error);
				response.send(false);
			})
		}else{
		response.send(false);
		}
}


//Retrieving All Active Products
module.exports.getAllActiveProducts = (request , response)=>{
	return Product.find({isActive: true}).then(result =>{
		response.send(result);
	}).catch(err =>{
		response.send(err);
	})
}


//Retrieving All Products
module.exports.getAllProducts = (request, response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	// console.log(userData);

	if(!userData.isAdmin){
			return response.send("Sorry! Only admin is Authorize to access this page!")
		}else{
			return Product.find({}).then(result => response.send(result))
			.catch(err =>{
			response.send(err);
		})
	}
}


//retrieving specific Product
module.exports.getSpecificProduct = (request, response) =>{
	const productId = request.params.productId;

	return Product.findById(productId).then(result => {
		response.send(result);
	}).catch(err => {
		response.send(err);
	})
}


//Archive/Unarchive Product
module.exports.archiveOrUnarchiveProduct =(request,response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);
	console.log(userData);

	let productArchive = {
		isActive : request.body.isActive,
	}

	const productId = request.params.productId;
	
	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, productArchive, {new: true}).then(result => {
			if (request.body.isActive === false) {
				response.json('archive');
			}else{
				response.json('unarchive');
			}
		}).catch(err =>{
			response.json(err);
		})
	}else{
		return response.json(null);
	}
}


//Update Products
module.exports.updateProduct=(request,response) =>{
	const token = request.headers.authorization;
	const userData = auth.decode(token);

	let updatedProduct= {
		productName : request.body.productName,
		description: request.body.description,
		price: request.body.price,
	}
	const productId = request.params.productId;

	if(userData.isAdmin){
		return Product.findByIdAndUpdate(productId, updatedProduct, {new: true}).then(result => {
			return response.send(result);
			console.log(result);
		}).catch(err =>{
			return response.send(null);
		})
	}else{
		return response.send(null);
	}
}


