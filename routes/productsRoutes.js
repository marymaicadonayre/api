const express = require ("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");
const auth = require("../auth.js");

//Routes in Adding/Creating Products
router.post("/addProduct", auth.verify,productControllers.addProduct);

//Routes in retreving all products
router.get("/allProducts", auth.verify, productControllers.getAllProducts);

//Routes in retreiving all active products
router.get("/allActiveProducts", productControllers.getAllActiveProducts);

//Retrieve Specific product
router.get("/getSpecificProduct/:productId", productControllers.getSpecificProduct);

//routes in Archiving/Unarchiving Products
router.patch("/archiveOrUnarchiveProduct/:productId", auth.verify, productControllers.archiveOrUnarchiveProduct);


//update specific Product
router.put("/updateProduct/:productId", auth.verify, productControllers.updateProduct);

module.exports = router;