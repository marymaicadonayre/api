const express = require ("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers");
const auth = require("../auth.js");


router.post("/addToCart/:productId", auth.verify,orderControllers.addToCart);

router.get("/allOrders", auth.verify, orderControllers.getAllOrders);

router.get("/allCarts", auth.verify, orderControllers.getOrderCart);

router.post("/checkOut", auth.verify, orderControllers.checkOut);



module.exports = router;