const express = require("express");
const router = express.Router();
const auth = require("../auth")

const userController = require("../controllers/userControllers")


	// route for registration
	router.post("/register", userController.checkEmailExists,userController.registerUser);

	// route for log in
	router.post("/login", userController.loginUser);
	
	//Add to cart
	// router.post("/addToCartOrder/:productId", auth.verify, userController.addToCartOrder);

	//Retrieve all users
	router.get("/allUsers", auth.verify, userController.getAllUsers);

	//ALL: get profile details
	router.get('/profile', userController.getProfileDetails);
	
	//Update role
	router.patch("/updateRole/:userId", auth.verify, userController.updateRole);


	

	

module.exports = router;